#lang r5rs

;;;; Exercice 5 : Listes chainées impératives (à rendre)

;;; Q1
(define (mk-list l)
  (let ((el (car l))
       (ls (cdr l)) ;elements strictement apres el
       (lp (list))) ;elements strictement avant el 
    (lambda (x)
      (case x
        ('value el)
        ('toliste l)
        ('has-next? (pair? ls))
        ('next! (begin
                  (set! lp (cons el))
                  (set! el (car ls))
                  (set! ls (cdr ls))))
        ('head! (begin
                  (set! el (car l))
                  (set! lp (cdr l))))
        ('del! (begin
                 (set! l  (append lp ls))
                 (set! el (car ls))
                 (set! ls (cdr ls))))))))
                  
;;; Q2

;;;; Exercice 6 : Listes plus efficaces (challenge)

;;; Q1

;;; Q2
