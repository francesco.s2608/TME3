;;;; Exercice 3 : Sous-séquence la plus longue
;;; longest : retourne la liste la plus longue entre
;;; l1 et l2
(define (longest l1 l2)
  (if (> (length l1)
         (length l2))
      l1 l2))
;;; lcs : retourne la sous-liste commune la plus
;;; longue entre l1 et l2
(define (lcs l1 l2)
  (cond
    ((null? l1) l1)
    ((null? l2) l2)
    ((equal? (car l1) (car l2))
     (cons (car l1) (lcs (cdr l1) (cdr l2))))
    (else
     (longest (lcs l1 (cdr l2))
              (lcs (cdr l1) l2)))))
(define (cons! e l)		; avec liste physiquement modifiee
  (set-cdr! l (cons (car l) (cdr l)))
  (set-car! l e))

(define (lcs-memo l1 l2 memo)
  (define x (or (assoc l1 memo) (assoc l2 memo)))
  (cond
    ((null? l1) l1)
    ((null? l2) l2)
    ((equal? (car l1) (car l2)) (if x
                                    (cons! (list l1 l2 (list (car l1) (cdr x))) memo )
                                    (cons (car l1) (lcs-memo (cdr l1) (cdr l2) memo))))
                    
    (else
     (longest (lcs-memo l1 (cdr l2) memo)
              (lcs-memo (cdr l1) l2 memo)))))

#lang r5rs
